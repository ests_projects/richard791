<?php
    /**
    * Initialization class
    * 
    * Created in order to create a safe environment with error handling and representation
    */
    class init
    {
        //var $repository;
        //var $database;
        //var $security;

        function __construct()
        {
            //session_start();

            // Call the autoloader so that when a class is called it is automatically included
            /*if(!function_exists('classAutoLoader')){
                function classAutoLoader($class){
                    $class=strtolower($class);
                    $classFile='custom-includes/'.$class.'.class.php';
                    if(is_file($classFile)&&!class_exists($class)) include $classFile;
                }
            }
            spl_autoload_register('classAutoLoader');
            */

            // Create the repository to save all the data
            //$this->repository = new repository();

            // Check which site the user is on
            /*$location = $_SERVER["PHP_SELF"];
            $location_items = explode("/", $location);
            $base = $location_items[1];
            $server = $_SERVER["SERVER_NAME"];*/

            // Load the config files for the different enviroments (dev / test/ live)
            /*if ($server == 'www.examenoverzicht.nl'){
                require_once('settings.live.class.php');
            }
            elseif ($server == 'www.exovt.nl'){
                require_once('settings.test.class.php');
            }
            else {
                require_once('settings.dev.class.php');
            }*/

            // Load the database class
            //$this->database = new database();

            // Load the security class
            //$this->security = new security($this->database);

            // Get the referral link and store it
            /*if (!isset($_SESSION['ref'])){
                if (isset($_GET['ref'])){
                    $_SESSION['ref']=$_GET['ref'];
                }
            }*/

        }
    }
?>
