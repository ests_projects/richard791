<?php
    /**
    * Viewer class
    * 
    * The product/ summary viewer module
    */
    class viewer
    {
        var $databaseClass;

        var $errors;
        var $succes;

        public function __construct($databaseClass)
        {
            if($databaseClass != "") {
                $this->databaseClass = $databaseClass;
            }
            else {
                $this->databaseClass = new database();
            }
        }

        /**
        * Retrieve a view url
        *
        * @access public
        * @param integer
        * @return string
        *
        */
        public function getViewUrl($productId,$viewerVersion){
            if ($productId && $viewerVersion) {
                $productId = $this->databaseClass->filter($productId);
                $viewerVersion = $this->databaseClass->filter($viewerVersion);

                // Check if the user actually purchased this summary
                $account = new account($this->databaseClass);
                $auth = $account->isProductViewAuthorized($productId);
                if ($auth == 'ok'){
                    $docId = $this->getDocumentId($productId);
                    if ($docId) {
                        $url = 'https://view-api.box.com/1/sessions';
                        $token = 'kc4rohmpg1f9bg91bui4p74nok8kyste';
                        $headers = array(
                            'Authorization: Token '. $token,
                            'Content-Type: application/json'
                        );

                        $data = array(
                            'document_id' => $docId,
                            'duration' => 1,
                            'is_downloadable' => 'false',
                            'is_text_selectable' => 'false'
                        );
                        $payload = json_encode($data);

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HEADER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                        $return = curl_exec($ch);
                        curl_close($ch);

                        $return_json = json_decode(strstr($return, '{'), true);

                        if ($viewerVersion == 'v1'){
                            $view_url = $return_json["urls"]["view"];
                        }
                        else {
                            $view_url = $return_json["urls"]["assets"];
                        }
                    
                        if ($view_url) {
                            return $view_url;
                        }
                        else {
                            // Viewer failed
                            $this->errors[] = "Het laden van de viewer is mislukt, probeer het nog een keer.";
                        }
                    }
                    else {
                        // No document
                        $this->errors[] = "Het document is niet gevonden. Neem contact met ons op via info@examenoverzicht.nl.";
                    }
                }
                else {
                    // User not authorized to view the product (or not digital)
                    $this->errors[] = "Het openen van de samenvatting is mislukt, de samenvatting is niet in je gekoppelde bestellingen gevonden.";
                }
            } 
            else {
                // Info not complete
                $this->errors[] = "Het openen van de samenvatting is mislukt, probeer het nog een keer.";
            } 
        }

        /**
        * Delete a view url
        *
        * @access public
        * @param string
        * @return string
        *
        */
        public function deleteViewUrl($viewUrl){
            if ($viewUrl) {
                $viewUrl = $this->databaseClass->filter($viewUrl);
                $url = 'https://view-api.box.com/1/sessions/'.$viewUrl;
                $token = 'kc4rohmpg1f9bg91bui4p74nok8kyste';
                $headers = array(
                    'Authorization: Token '. $token,
                    'Content-Type: application/json'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $return = curl_exec($ch);
                curl_close($ch);

                if ($return) {
                    return $return;
                }
                else {
                    return false;
                }
            } 
            else {
                // 
            } 
        }

        /**
        * Retrieve a document id
        *
        * @access public
        * @param integer
        * @return string
        *
        */
        public function getDocumentId($productId){
            if ($productId) {
                $productId = $this->databaseClass->filter($productId);

                $query = "SELECT
                P.doc_id
                FROM 
                Products P
                WHERE P.id=".$productId.";";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }  
        }

    }
?>
